// Асинхронный код позволяет выполнять код без подряда, как обычно, а дождаться выполнения отдельных операций и затем продолжить работу с получениемы данными.

const ipButton = document.querySelector('.ip')
const ipSet = document.querySelector('.ip2')

const continentName = document.querySelector('.continent__name')
const countryName = document.querySelector('.country__name ')
const regionCall = document.querySelector('.region__name')
const cityName = document.querySelector('.city__name')
const districtName= document.querySelector('.district__name')

async function findIp() {
    try {
        const res = await fetch('https://api.ipify.org/?format=json')
        const { ip } = await res.json()
        ipValue(ip)
        getLocation(ip)
    } catch (error) {
        console.log(error)
    }
}

async function getLocation(ip) {
    try {
        const res = await fetch(
            `http://ip-api.com/json/${ip}?fields=status,continent,country,regionName,city,district`
        );
        const info = await res.json()
        findLocation(info)
    } catch (error) {
        console.log(error)
    }
}

function findLocation({ continent, country, regionName, city, district }) {
    continentName.innerText = continent
    countryName.innerText = country
    regionCall.innerText = regionName
    cityName.innerText = city
    districtName.innerText = district
}

function ipValue(ip) {
    ipSet.innerText = ip
}

ipButton.addEventListener('click', findIp )
